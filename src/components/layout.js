/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
import "./layout.css"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <div class="js-c-page">
      <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
      <div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0 1.0875rem 1.45rem`,
        }}
      >
        <main
          className="js-c-page__content"
          style={{
            display: `flex`,
            flexDirection: `column`,
            minHeight: `60vh`,
            flex: 1,
            margin: `4rem 0`,
          }}
        >
          {children}
        </main>
      </div>
      <footer
        style={{
          marginTop: `2rem`,
          padding: `3rem 0`,
          color: `white`,
          background: `rebeccapurple`,
        }}
      >
          <div
            style={{
              margin: `0 auto`,
              maxWidth: 960,
              padding: `0 1.0875rem 1.45rem`,
            }}
          >
            © {new Date().getFullYear()}, Built with
          {` `}
            <a
              style={{
                color: `white`,
              }}
              href="https://www.gatsbyjs.com"
            >
              Gatsby
            </a>
          </div>
        </footer>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
