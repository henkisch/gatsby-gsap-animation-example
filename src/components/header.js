import TransitionLink from "gatsby-plugin-transition-link";
import PropTypes from "prop-types"
import React from "react"
import {
  transitionPageIn,
  transitionPageOut,
} from './animations';

const Header = ({ siteTitle }) => (
  <header
    style={{
      background: `rebeccapurple`,
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
        display: `flex`,
        justifyContent: `space-between`,
        alignItems: `center`
      }}
    >
      <h1 style={{ margin: 0 }}>
        <TransitionLink
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
          exit={{
            length: 1.5,
            trigger: () => {
              transitionPageOut(0.5);
            },
          }}
          entry={{
            delay: 1.5,
            length: 1.5,
            trigger: () => {
              transitionPageIn(0);
            },
          }}
        >
          {siteTitle}
        </TransitionLink>
      </h1>
      <div>
        <TransitionLink
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
          to="/page-2/"
          exit={{
            length: 1.5,
            trigger: () => {
              transitionPageOut(0.5);
            },
          }}
          entry={{
            delay: 1.5,
            length: 1.5,
            trigger: () => {
              transitionPageIn(0);
            },
          }}
        >
          Go to page 2
        </TransitionLink>
      </div>  
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
