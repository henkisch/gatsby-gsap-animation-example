import { gsap } from 'gsap';

// ---- Default page transition in ---- //
export const transitionPageIn = (customDelay = 0) => {
  const tl = gsap.timeline({ delay: customDelay });
  const page = '.js-c-page';
  const pageContent = '.js-c-page__content';

  // Remove initial flash.
  tl.to(page, 0, { css: { visibility: 'visible' } });

  tl.to(pageContent, 0, {
    css: {
      y: 100,
      skewY: -3,
      opacity: 0,
    },
  });

  tl.to(pageContent, {
    duration: 1,
    css: {
      y: 0,
      opacity: 1,
      skewY: 0,
    },
    ease: 'Power4.easeInOut',
  }, 'Start')
    .to(page, {
      delay: 0.375,
      css: {
        overflowY: 'visible',
      },
    }, 'Start');
};

// ---- Default page transition out ---- //
export const transitionPageOut = (customDelay = 0) => {
  const tl = gsap.timeline({ delay: customDelay });
  const page = '.js-c-page';
  const pageContent = '.js-c-page__content';

  tl.to(pageContent, {
    duration: 1,
    css: {
      y: 100,
      skewY: -3,
    },
    ease: 'Power4.easeInOut',
  }, 'Start')
    .to(pageContent, {
      delay: 0.25,
      duration: 0.75,
      css: {
        opacity: 0,
      },
    }, 'Start')
    .to(page, {
      css: {
        overflowY: 'hidden',
      },
    }, 'Start');
};